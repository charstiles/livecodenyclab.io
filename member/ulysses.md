---
name: Ulysses Popple
image: https://en.gravatar.com/avatar/05e9f304f29113e7155df3ab1f1d21e8.jpg?size=800
links:
    website: https://ulyssesp.com/
---

Former actor, active coder. Loves live performance of all kinds and makes visual art by programming.