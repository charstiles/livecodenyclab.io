---
name: Amit Nambiar
image: https://avatars2.githubusercontent.com/u/15354742?s=460&u=3209e7cfdbb6a0d35bb8d8ff7e8ddc5da4b05688&v=4
links:
    website: https://amitnambiar.com/livecode
---

Computational designer interested in generative 3d environments.
