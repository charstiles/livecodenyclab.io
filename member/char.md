---
name: Char Stiles
image: http://charstiles.com/wp-content/uploads/2022/02/49564850578_46062e6011_k.jpg
links:
    website: http://charstiles.com
    twitter: https://twitter.com/CharStiles
    instagram: https://www.instagram.com/charstiles/
---

Computational artist, livecoding shaders & giving workshops. Thinking about email.