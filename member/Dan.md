---
name: Dan Gorelick
image: https://i0.wp.com/grayarea.org/wp-content/uploads/2022/02/dan_gorelick_headshot_portrait-scaled-e1645738120469.jpg
links:
    website: https://dan.dog/
    twitter: https://twitter.com/dqgorelick
    instagram: https://www.instagram.com/dqgorelick/
---

Musician and creative coder. Creates ghost pianos with code.