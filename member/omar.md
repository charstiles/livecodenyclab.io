---
name: Omar Delarosa
image: https://s3.amazonaws.com/omardelarosa.com/assets/images/waves.jpg
links:
    website: https://omardelarosa.com/
---

A curious nerd in the process of exploring the overlap between computer science and music.
