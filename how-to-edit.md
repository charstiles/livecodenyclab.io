---
title: How to Edit
---

# How to Contribute

The source code for this website can be found on [GitLab](https://gitlab.com/livecodenyc/livecodenyc.gitlab.io). LiveCode.NYC members are invited to submit Pull Requests to keep information up to date.

## Getting Started

### Requirements

* [GitLab Account](https://gitlab.com/users/sign_up)
* [Git Command Line Tool](https://git-scm.com/downloads)
* Code Editor (e.g. [Atom](https://atom.io))
* Terminal Emulator (e.g. [iTerm2](https://iterm2.com))

### Make a Fork

In order to submit *Pull Requests*, you will need to have a personal fork of the LiveCode.NYC website repository. 

1. Sign into your GitLab account
2. Go to [https://gitlab.com/livecodenyc/livecode.nyc/](https://gitlab.com/livecodenyc/livecode.nyc/)
3. Click "Fork" in the upper right hand corner. 
4. Click your profile to fork to your personal GitLab namespace.

### Clone repository to your desktop

1. Open your Terminal
2. Type the following command `git clone https://gitlab.com/[YOUR USERNAME]/livecode.nyc.git` and replace [YOUR USERNAME] with your GitLab username.
3. Move into the newly created repository with `cd livecode.nyc`

### Add your profile to the member list

1. Make a new file in the `/member` folder with your name (e.g. `jane.md`)
2. Use the following template to make your profile page 
```yaml
---
name: [YOUR NAME]
image: [URL TO IMAGE FILE]
links:
    website: [YOUR WEBSITE URL]
    twitter: [YOUR TWITTER URL]
---

[YOUR BIO (ONE TO TWO SENTENCES)]
```
3. Open your terminal and navigate to the `livecode.nyc` folder.
4. Commit your changes with `git add . && git commit -m "Adding jane.md" && git push origin master`

### Create pull request

