---
title: Design Assets
---

# Design Assets

The LiveCode.NYC logo was designed by [David Palmer](https://www.dp-creative.co.uk/)
<br/><br/>

## LiveCode.NYC Icon

<br/>
<div style="display:inline-block;margin-right:1rem;margin-bottom:1rem;text-align:center;">
<div style="padding:20px;background:#1D1D1B;">
<img src="/media/logos/LiveCodeNYC_logo_white.svg" height="100">
</div>

[.SVG](/media/logos/LiveCodeNYC_logo_white.svg),
[.AI](/media/logos/LiveCodeNYC_logo_white.ai),
[.EPS](/media/logos/LiveCodeNYC_logo_white.eps)

</div>
<div style="display:inline-block;text-align:center;color:#1D1D1B;margin-bottom:1rem;">
<div style="background:#fff; padding:20px;">
<img src="/media/logos/LiveCodeNYC_logo_black.svg" height="100">
</div>

<a href="/media/logos/LiveCodeNYC_logo_black.svg">.SVG</a>,
<a href="/media/logos/LiveCodeNYC_logo_black.ai">.AI</a>,
<a href="/media/logos/LiveCodeNYC_logo_black.eps">.EPS</a>

</div>
<br/><br/>

## Colors

We have used these colors for our logo and promotional materials in the past but feel free to be creative!

<div style="padding:20px;display:inline-block;margin:1rem 1rem 1rem 0; font-size:0.8em;background-color:rgba(0,0,51,1.0);color:#ffffff; min-width:300px">
Deep Blue
<br><br>
<span class="mono">RGBA(0, 0, 51, 1.0)</span>
<br>
<span class="mono">vec4(0.0, 0.0, 0.2, 1.0)</span>
<br>
<span class="mono">#000033</span>
<br>
<span class="mono">94% C, 82% M, 20% Y, 49% K</span>
</div>
<div style="padding:20px;display:inline-block;margin:1rem 1rem 1rem 0; font-size:0.8em;background-color:rgba(51,255,255,1.0);color:#000000; min-width:300px">
Electric Blue
<br><br>
<span class="mono">RGBA(51, 255, 255, 1.0)</span>
<br>
<span class="mono">vec4(0.2, 1.0, 1.0, 1.0)</span>
<br>
<span class="mono">#33FFFF</span>
<br>
<span class="mono">39% C, 0% M, 13% Y, 0% K</span>
</div>
<div style="padding:20px;display:inline-block;margin:1rem 1rem 1rem 0;
font-size:0.8em;background-color:rgba(153,255,0,1.0);color:#000000; min-width:300px">
Electric Green
<br><br>
<span class="mono">RGBA(153, 255, 0, 1.0)</span>
<br>
<span class="mono">vec4(0.6, 1.0, 0.0, 1.0)</span>
<br>
<span class="mono">#99FF00</span>
<br>
<span class="mono">29% C, 0% M, 65% Y, 0% K</span>
</div>
<div style="padding:20px;display:inline-block;margin:1rem 1rem 1rem 0;
font-size:0.8em;background-color:#FF0099;color:#000000; min-width:300px">
Electric Pink
<br><br>
<span class="mono">RGBA(255, 0, 153, 1.0)</span>
<br>
<span class="mono">vec4(1.0, 0.0, 0.6, 1.0)</span>
<br>
<span class="mono">#FF0099</span>
<br>
<span class="mono">7% C, 73% M, 1% Y, 0% K</span>
</div>
