---
name: October Algorave
venue: H0lo
date: 2017-08-29 7:00 PM
image: https://scontent-lga3-2.xx.fbcdn.net/v/t31.0-8/21950004_1778609479106308_2368035120924929596_o.png?_nc_cat=102&ccb=2&_nc_sid=340051&_nc_ohc=zxj04d7mUfsAX_0k_mO&_nc_ht=scontent-lga3-2.xx&oh=c8ac324bd71bc303169e469c8ebbabb8&oe=6004D001
address: 
---

BASS. BEATS. BOOLEANS.
An evening of live coding music featuring Parrises Squares, Vinton Surf, Daniel Steffy and Reckoner.
Visuals by ¸¸.•*¨*• Joseph Gordon x ART404 •*¨*•.¸¸
Tickets at the door.

MORE ABOUT ALGORAVE.
https://algorave.com/about/
SEE WHAT WE DO.
https://vimeo.com/228411462