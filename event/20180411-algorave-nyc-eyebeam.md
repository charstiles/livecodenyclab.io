---
name: Algorave NYC
venue: Eyebeam
date: 2018-04-11 6:00 PM
image: https://scontent-lga3-2.xx.fbcdn.net/v/t1.0-9/29104304_10156245763258756_1095649966845591552_o.jpg?_nc_cat=111&ccb=2&_nc_sid=340051&_nc_ohc=bPitQXHGdckAX9_wioA&_nc_ht=scontent-lga3-2.xx&oh=73a686697ea828358d3843d9af9bbaee&oe=60048F52
address: 
---

[ Eyebeam's Welcome Wednesday ]
Algorave, Hosted by Ramsey Nasser and LiveCode.NYC
April 11, 2018 / 6 – 11pm
Join us for an evening of live coded sounds and visuals made for dancing. Live coders program and change their computational systems as they are running, to produce real-time creative coded works.
Featuring:
2050
Ulysses Popple
Sean Lee
Michael Lee
Codie
Charlie Roberts
Kindohm
Ramsey Nasser
Free and open to the public, make sure to RSVP!
___
This event is part of Welcome Wednesday, our ongoing event series that offers a flexible, social time and space for current and former Eyebeam artists to share new ideas, works in progress, host conversations, and experiment with different formats.